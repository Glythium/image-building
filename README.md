# Image Building

This is a project designed to automate the building of lightweight, Alpine-based, container images. The automated CI/CD pipeline runs every day to ensure that the images built by this project are fresh.

### Why Would I Use This?
If you've been working with containers for a while, you've probably run into a scenario where you can't find a "perfect" container to fit your use case on any of the major container registries. This project aims to make it easy to reliably build your own containers.

### Getting Started

**Prerequisites:**
- A Gitlab CI/CD Runner registered with this project to execute the pipeline
- A container registry to store your custom containers in

**Setup:**
1. Follow the steps listed in [the image-builder README](custom-images/image-builder/README.md) to create the image that will be building your custom images

1. Set your repository's CI/CD variables to the following values or set up the Harbor integration to get these defined that way instead
```yaml
HARBOR_HOST: the FQDN of the container registry you want to store your custom images in
HARBOR_USERNAME: the username that you'll use to authenticate to that registry
HARBOR_PASSWORD: the password that you'll use to authenticate to that registry
HARBOR_PROJECT: the name of the Project in the registry that you want to store the images in
```

**Defining your own container:**
1. Edit the `custom-images/images.yaml` file by following the YAML schema to define your custom container.

1. Run the pipeline
