#!/bin/sh

cd "${CI_PROJECT_DIR}/custom-images"

# Login to the registry
crane auth login "${HARBOR_HOST}" \
    --username "${HARBOR_USERNAME}" \
    --password "${HARBOR_PASSWORD}"

# Build all of the iamges listed in the images.yaml
for i in $(seq 0 $(yq '.custom-images | length' images.yaml)); do

    # Get the name value from the images.yaml file
    name=$(yq ".custom-images[${i}].name" images.yaml);
    if [[ "${name}" == "null" ]]; then
        # This is a required value, do not attempt to build an image with no name
        continue;
    fi;

    # Get the packages value from the images.yaml file
    packages=$(yq ".custom-images[${i}].packages" images.yaml);
    if [[ "${packages}" == "null" ]]; then
        # This is an optional value
        packages=""
    fi;

    # Get the home value from the images.yaml file
    home=$(yq ".custom-images[${i}].home" images.yaml);
    if [[ "${home}" == "null" ]]; then
        # This is an optional value
        home="1001"
    fi;

    echo "${build_args}"

    # Create a tarball of the image
    /kaniko/executor \
        --context . \
        --dockerfile Dockerfile.template \
        --build-arg packages="${packages}" \
        --build-arg home="${home}" \
        --no-push \
        --tar-path "./${name}.tar";
    
    # Push the image
    crane push "./${name}.tar" "${HARBOR_HOST}/${HARBOR_PROJECT}/${name}:latest";
    
    # Clean up that tarball to tidy up before the next iteration of the loop
    rm "./${name}.tar";
done