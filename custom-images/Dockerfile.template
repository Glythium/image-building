FROM alpine

ARG home="1001"
ARG packages=""

# We'll use root for a bit to set up the container environment
USER root

# Update all the packages available on the system, prune unused packages, invalidate the package cache
RUN apk update && apk upgrade --prune --force-refresh

# Install the packages listed in the config file
RUN apk add ${packages}

# Create the user's new home directory in the /opt folder to avoid overwriting existing root directories
RUN mkdir -p /opt/${home}

# Create our non-privileged user account named 1001
RUN adduser -HD -h /opt/${home} -u 1001 1001

# Switch to the non-privileged user
USER 1001

# Set the working directory to the non-privileged user's home folder
WORKDIR /opt/${home}

ENTRYPOINT [ "sh" ]